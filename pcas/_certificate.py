"""
Defines the PCAS root SSL certificate.

"""

# This software is distributed under an MIT license. You should have received a 
# copy of the license along with this software. If not, see 
# <https://opensource.org/licenses/MIT>.


# The PCAS root SSL certificate. 
PCAS_ROOT_CERTIFICATE = b"""-----BEGIN CERTIFICATE-----
MIIG2DCCBMCgAwIBAgIVAPAatPA6gLeWb4FkQe8ynH24MPx5MA0GCSqGSIb3DQEB
CwUAMIHzMQswCQYDVQQGEwJHQjEXMBUGA1UECBMOR3JlYXRlciBMb25kb24xGTAX
BgNVBAcTEFNvdXRoIEtlbnNpbmd0b24xGDAWBgNVBAkTD0V4aGliaXRpb24gUm9h
ZDEQMA4GA1UEERMHU1c3IDJBWjE1MDMGA1UEChMsUGFyYWxsZWwgQ29tcHV0YXRp
b25hbCBBbGdlYnJhIFN5c3RlbSAoUENBUykxIjAgBgNVBAsTGURlcGFydG1lbnQg
b2YgTWF0aGVtYXRpY3MxKTAnBgNVBAMTIDA1YWE1NzQ4YjY3YTQ3MDRmYTAyOGQw
MjQ5ZGUyZDFkMB4XDTIyMDIyMzIwNDIwNVoXDTMyMDIyMzIwNDIwNVowgfMxCzAJ
BgNVBAYTAkdCMRcwFQYDVQQIEw5HcmVhdGVyIExvbmRvbjEZMBcGA1UEBxMQU291
dGggS2Vuc2luZ3RvbjEYMBYGA1UECRMPRXhoaWJpdGlvbiBSb2FkMRAwDgYDVQQR
EwdTVzcgMkFaMTUwMwYDVQQKEyxQYXJhbGxlbCBDb21wdXRhdGlvbmFsIEFsZ2Vi
cmEgU3lzdGVtIChQQ0FTKTEiMCAGA1UECxMZRGVwYXJ0bWVudCBvZiBNYXRoZW1h
dGljczEpMCcGA1UEAxMgMDVhYTU3NDhiNjdhNDcwNGZhMDI4ZDAyNDlkZTJkMWQw
ggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQDNoBZGl+BUzgxHfkmjFgtN
Oikx1A5qHRNwvo1fULgggEjnWvUNVXtBprDOyhEjawCC09jYCoevXLhOugFvb5Rq
qJ5OGrLYLOdkEYLxiHGbv5v2vboLNw0hBrpzLDSTPeeL0K+7Op7EnmUMnD2CACxF
gnxMqvxfBQeCTAWcq3Fzxpfxtc5Svl3E4LdTDbSE26tqn2yMBB60RGPxHjtjJ0oU
18htOTszANlz10GErJjboEKWgtjApOmKx6YFApYL7PelWCV3hBA9l4FY52+xPcBT
a6KLA1kVFaV3Dlt8i5L23UKYRrnolCJI746DTdFCGaKhy8FJwJXEv6fl0abM7WaO
QMAnuPx9O+wFo94jmVVlGu1TNVKbcBlnkskVA1e8sfKMMIY82DpfHKh+Po1MSo7D
P/y9437cQQUJd23Pkv7oMoAUX+427/PMFjj3aL9CAYrRTQg6182CcvqVKsWVgHLH
WdGXlt8NlOlJGvSHmLS4zsRZCti0P2OWHhkJ9Q0+wifKV6E+0I2A1u73J1t6Frnw
+ENHgNRWjxJCtxZyi7hYBCjZV9AVgHoR1mbHKzCtEWay7piwGpp0KneT4au5EmDs
1TD054Ii+66jlE8UkbuqpR2NU86RaDVdmmG++z8FJdZTJdj0fkp4IU/TI9QbRKD/
thwevxKFT5xeyysUZGF0YQIDAQABo2EwXzAOBgNVHQ8BAf8EBAMCAoQwHQYDVR0l
BBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0O
BBYEFJWaiE6g+vsSAPIqJqaOzpC6Bo25MA0GCSqGSIb3DQEBCwUAA4ICAQBhmK3M
ZUFMHeqBCn71X30Us83llPArZhbiMCibke1z8EH6fGIuWwNIJrrbfMFb4eMjn2KZ
ZBE1WGfGALAk46rsnUKbA12c+r9+ecpjKQN+D3jgHOeeBrjOjoAzNTSL/y63/+Mw
0hwp5O7OMKI3PSiQKVFPYZ/UZCNhsGAz0Ym55fxFb7JoM6su+dO4ADcgleue9zYI
F2/lMvccribTM/Ui13i7IX9prD1v05ZRJpKNf/TACQ9+kjc6OQ9qFbk1ZeT6Lkpu
gx5OyoKlx/gDwWzy03On2Pswue1kDAF6EQY4mwf03eiILZiSnjPpK2yv6FZy5qN2
v3DmNixJnH8Gy2tx89AbcPhdwK82mJG4infLMGRxU8CuoYNuSKEDg3coDbJDJ7EF
YfkrqJbmLjuTSbu5za5PN+wrsKq6L0OkXt/X+BTMjJO9Po4wmR7TOXCry/6VP0Pj
iwKy9GDO/glnP4xP9w+MeIu0QX2itZO104JlnTsSLupfUxfzRQikk4ZMZFLIEEDJ
liHe5KoLylxkZCjIQhONB6mCRxd4UAhhB53DlkjnLHmtSkioTKztGvKgiuaTcdBw
AMG48cvHSO7UHdB4D+4f2zp9pn4zQhty986LSoNGElbDhvVvkFgmEIHc6HNYt6ay
rCKM4vC7GGOBs6GS7UVY5pLoyvus6W8PLChaQQ==
-----END CERTIFICATE-----
"""