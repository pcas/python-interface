"""
Defines functions for processing key-value records.
"""

# This software is distributed under an MIT license. You should have received a 
# copy of the license along with this software. If not, see 
# <https://opensource.org/licenses/MIT>.

from ._record import (
    is_key,
    is_value,
)

__all__ = [
    "is_key",
    "is_value",
]
