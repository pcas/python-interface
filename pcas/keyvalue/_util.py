"""
Utility functions for the keyvalue package.
"""

# This software is distributed under an MIT license. You should have received a 
# copy of the license along with this software. If not, see 
# <https://opensource.org/licenses/MIT>.

def _validate_table_name(s):
    """
    Validates the given table name.
    
    Args:
        s (str):
            the table name

    Raises:
        ValueError:
            if validation fails
    
    Returns:
        None
    
    """
    if not isinstance(s, str):
        raise TypeError("the table name should be a string")
    if not s:
        raise ValueError("the table name cannot be empty")

def _validate_key(s):
    """
    Validates the given key.
    
    Raises:
        ValueError:
            if validation fails
    
    Returns:
        None
    
    """
    if not isinstance(s, str):
        raise TypeError("the key should be a string")
    if not s:
        raise ValueError("the key cannot be empty")
