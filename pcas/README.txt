To rebuild the auto-generated gRPC files for the subpackage with name $SUBPACKAGE_NAME, from the python-interface directory, do

python3 -m grpc_tools.protoc -I pcas/$SUBPACKAGE_NAME/proto --python_out=. --grpc_python_out=. pcas/$SUBPACKAGE_NAME/proto/pcas/$SUBPACKAGE_NAME/*.proto

