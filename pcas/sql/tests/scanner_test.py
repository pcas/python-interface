"""
Provides tests for _scanner.py

"""

# This software is distributed under an MIT license. You should have received a 
# copy of the license along with this software. If not, see 
# <https://opensource.org/licenses/MIT>.

import unittest
from .._scanner import _Scanner
from .._lexer import _Tokeniser
from ..exceptions import ScanError


class TestScanner(unittest.TestCase):

    def test_scanner_error(self):
        s = "a perfectly valid string"
        t = _Tokeniser(_Scanner(s))
        next(t) # no exception
        s = "\u0394 a string with an invalid leading character"
        t = _Tokeniser(_Scanner(s))
        self.assertRaisesRegex(ScanError, "invalid character \[near offset 0\]", lambda: next(t))

    def test_position(self):
        t = _Scanner("")
        self.assertEqual(t.position(), 0)
        s = "a valid string"
        self.assertEqual(t.position(), 0)


if __name__ == '__main__':
    unittest.main()