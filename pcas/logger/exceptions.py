"""
Defines exceptions for the logger package.

"""

# This software is distributed under an MIT license. You should have received a 
# copy of the license along with this software. If not, see 
# <https://opensource.org/licenses/MIT>.

class HandlerClosedError(Exception):
    """Exception raised when trying to log to a closed handler."""
    pass