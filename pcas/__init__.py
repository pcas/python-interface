from ._certificate import PCAS_ROOT_CERTIFICATE

__all__ = [
    "PCAS_ROOT_CERTIFICATE"
]